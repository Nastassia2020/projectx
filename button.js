export function createButton(name, handler){
    let button = document.createElement('button');
    button.className = 'forButton';
    button.textContent = name;
    return button;
}
