import {createButton} from '/button.js';
//import {createInput} from './input.js';
import * as global from './global.js';
import * as game from './game.js';
import * as welcome from './welcome.js';
//import * as newClient from './createClient.js';
//import * as newPerson from './newPerson.js';

const div = document.getElementById('root');
div.className = 'rootIMG';
const header = document.createElement('div');
header.className = 'forHeader';
const registration = createButton('Регистрация');
const enter = createButton('Вход');
const play = createButton('Хочу играть!');
const closeGame = createButton('Закрыть игру');
closeGame.style.opacity = '0';
play.style.opacity = '0';
header.append(enter, registration, play, closeGame);
div.appendChild(header);
const exit = createButton('Exit');
exit.className = 'exit';
div.append(exit);

const modal = document.createElement('div');
modal.className = 'forModal';
div.appendChild(modal);
const modalClose = document.createElement('div');
modalClose.innerHTML = '<p data-close="true">&times;</p>';
modalClose.className = 'modalClose';
modalClose.setAttribute('data-close', 'close');

//Функции для открытия-закрытия игры
function openGame() {
    modal.classList.add('open');
    game.map.classList.add('open');
    registration.style.opacity = '0';
    enter.style.opacity = '0';
    play.style.opacity = '0';
    closeGame.style.opacity = '1';
}

function closeGameButton() {
    modal.classList.remove('open');
    game.map.classList.remove('open');
    registration.style.opacity = '0';
    enter.style.opacity = '0';
    play.style.opacity = '1';
    closeGame.style.opacity = '0';
}

play.addEventListener('click', openGame);
game.map.addEventListener('click', game.eventGame);
modal.appendChild(game.map);
closeGame.addEventListener('click', closeGameButton);

//Создаёт форму регистрации
function createClient(){
    const form = document.createElement('div');
    form.className = 'forReg';

    let formH1 = document.createElement('h1');
    formH1.innerText = 'Регистрация';
    formH1.className = 'forH1';

    global.login.setAttribute('placeholder','Придумайте логин');
    global.pass1.setAttribute('placeholder', 'Придумайте пароль');
    global.pass2.setAttribute('placeholder', 'Повторите пароль');

    global.login.className = 'forInput';
    global.pass1.className = 'forInput';
    global.pass2.className = 'forInput';

    const buttonInput = document.createElement('div');
    global.save.innerText = 'Сохранить';
    global.cancel.innerText = 'Отмена';
    global.save.className = 'forButton';
    global.cancel.className = 'forButton';
    buttonInput.append(global.save, global.cancel);
    buttonInput.className = 'forButtonInput';

    form.append(modalClose, formH1, global.login, global.pass1, global.pass2, buttonInput);
    modal.appendChild(form);
    return form
}

//Создаёт форму для входа существующего пользователя
function createEnter(){
    const client = document.createElement('div');
    client.className = 'forReg';

    const clientH1 = document.createElement('h1');
    clientH1.innerText = 'Авторизация';
    clientH1.className = 'forH1';

    global.name.setAttribute('placeholder','Введите логин');
    global.password.setAttribute('placeholder','Введите пароль');
    global.name.className = 'forInput';
    global.password.className = 'forInput';

    const buttonInputEnter = document.createElement('div');
    global.enter.innerText = 'Войти';
    global.cancel.innerText ='Очистить';
    global.enter.className = 'forButton';
    global.cancel.className = 'forButton';
    buttonInputEnter.append(global.enter, global.cancel);
    buttonInputEnter.className = 'forButtonInput';

    client.append(modalClose, clientH1, global.name, global.password, buttonInputEnter);
    modal.appendChild(client);
    return client
}

function openModal(){
    const $modal = createClient()
    modal.classList.add('open');
    $modal.classList.add('open');
}

function clearModal(){
    global.login.value = '';
    global.pass1.value = '';
    global.pass2.value = '';
    global.login.style.borderColor = 'grey';
    global.pass1.style.borderColor = 'grey';
    global.pass2.style.borderColor = 'grey';
}

function closeModal() {
    const $modal = createClient();
    modal.classList.remove('open');
    $modal.classList.remove('open');
    clearModal();
}

registration.addEventListener('click', openModal);
global.cancel.addEventListener('click', clearModal);
modalClose.addEventListener('click', closeModal);

function openEnter(){
    const $modal = createEnter();
    modal.classList.add('open');
    $modal.classList.add('open');
}

function clearEnter(){
    global.name.value = '';
    global.password.value = '';
    global.name.style.borderColor = 'grey';
    global.password.style.borderColor = 'grey';
}

function closeEnter() {
    const $modal = createEnter();
    modal.classList.remove('open');
    $modal.classList.remove('open');
    clearEnter();
}

enter.addEventListener('click', openEnter);
modalClose.addEventListener('click', closeEnter);
global.cancel.addEventListener('click', clearEnter);

class User {
    constructor(options){
        this.name = global.login.value,
        this.password = global.pass1.value
    }

}

//Проверяет валидность логина и паролей, впускает пользователя в игру + создаёт объект пользователя
function check(){
    for(let i = 0; i <= localStorage.length; i++) {
        let userKey = localStorage.key(i);
        if(global.login.value === userKey){
        alert("Имя пользователя уже существует");
        clearModal();
    }
    } if(global.login.value === ' ' || global.login.value.length < 4 || global.login.value.length > 10) {
        alert("Имя должно содержать от 4х до 10 символов!");
        global.login.style.borderColor = 'red';
        return null;
    } else if(global.pass1.value === ' ' || global.pass1.value.length < 6 || global.pass1.value.length > 10){
        alert("Длина пароля должна быть от 6 до 10 символов!");
        global.pass2.style.borderColor = 'red'; global.pass1.style.borderColor = 'red';
    } else if(global.pass1.value !== global.pass2.value){
        alert("Пароли не совпадают!");
        global.pass2.style.borderColor = 'red';
    }else {
        class people extends User {};
        let user = new people;
        localStorage.setItem(user.name, JSON.stringify(user));
        openEnter();
    }
}

global.save.addEventListener('click', check);

//Проверяет корректность введённых данных при входе пользователя
function checkEnter(){
    const person = global.name.value;
    const carrentPass = global.password.value;
    const personString = localStorage.getItem(person) || '{}';
    const personObject = JSON.parse(personString);
    if(carrentPass !== personObject.password) {
        alert("Вы ввели неверный логин или пароль");
    }else {
        localStorage.setItem("person",JSON.stringify(person));
        closeEnter();
        welcome.welcomePerson(person);
        play.style.opacity = '1';
        registration.style.opacity = '0';
        enter.style.opacity = '0';
        location.reload();
    }
}

global.enter.addEventListener('click', checkEnter);

//Оставляет пользователя вошедшим, после перезагрузки страницы
function logged(){
    const pers = localStorage.getItem('person');
    if(pers){
        welcome.welcomePerson(pers);
        play.style.opacity = '1';
        exit.style.opacity = '1';
        registration.style.opacity = '0';
        enter.style.opacity = '0';
    }
}
logged();

//Удаляет вошедшего пользователя и обновляет всё приложение
function logOut(){
    localStorage.removeItem('person');
    play.style.opacity = '0';
    exit.style.opacity = '0';
    registration.style.opacity = '1';
    enter.style.opacity = '1';
    location.reload();
}

exit.addEventListener('click', logOut);



