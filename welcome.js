import * as global from './global.js';
import {createButton} from '/button.js';

const div = document.getElementById('root');
div.className = 'rootIMG';
export const welcome = document.createElement('p');
welcome.className = 'welcome';
// const exit = createButton('Exit');
// exit.className = 'exit';

export function welcomePerson(person) {
    welcome.innerText = "Hello, my dear " + person + "!";
    div.append(welcome);
}