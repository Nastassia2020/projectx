export const map = document.createElement('div');
map.className = 'map';
export const message = document.createElement('div');
message.className = 'message';
map.appendChild(message);

export let getRandomNumber = function(size) {
    return Math.floor(Math.random() * size);
}

export const getDistance = function(event, target){
    let diffX = event.offsetX - target.x;
    let diffY = event.offsetY - target.y;
    return Math.sqrt((diffX * diffX) + (diffY * diffY));
}

export const getDistanceHint = function(distance) {
    if(distance < 25) {
        return "Oh, so hot!!!!";
    } else if(distance < 50) {
        return "Very hot!";
    } else if(distance < 80) {
        return "Hot!";
    }
    else if(distance < 100) {
        return "Very warm!";
    } else if(distance < 140) {
        return "Warm!";
    } else if(distance < 180) {
        return "Mmm, cold!";
    } else if(distance < 260) {
        return "Brrrr, cold!";
    } else if(distance < 460) {
        return "Very cold!";
    } else {
        return "Were is my coat dude???";
    }
}

export let width = 500;
export let height = 500;
export let clicks = 0;

export let target = {
    x: getRandomNumber(width),
    y: getRandomNumber(height)
}

export function eventGame() {
    clicks++;
    let distance = getDistance(event, target);
    let distanceHint = getDistanceHint(distance);
    message.innerText = distanceHint;
    if(distance < 10) {
        alert("Super! You are so precision!");
    }
}


